﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    float speed = 0.5f;
    float turnSpeed = 1.0f;
    public bool dead = false;
    GameObject home;
    Animator anim;
    bool lookingForHome = true;


    void Start()
    {
        anim = this.GetComponent<Animator>();
        InvokeRepeating("FindHome", 0, 1);
    }

    void FindHome() 
    {
        home = GameObject.FindGameObjectWithTag("Home");
        if (home != null)   
        {
            CancelInvoke();
            lookingForHome = true;
        }
            
    }

    void Update()
    {
        if (dead) return;

        if (home != null) 
        {
            Vector3 direction = home.transform.position - this.transform.position;
            this.transform.rotation = Quaternion.Slerp(
                this.transform.rotation, Quaternion.LookRotation(direction),
                turnSpeed * Time.smoothDeltaTime);
        }
        else if (lookingForHome) 
        {
            InvokeRepeating("FindHome", 0, 1);
            lookingForHome = false;
        }
        
        this.transform.LookAt(home.transform.position);

        this.transform.Translate(0, 0, speed * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Home") 
        {
            this.GetComponent<AudioSource>().Play();
            Hit();

        }

        else if (other.gameObject.tag == "Bullet")
            Hit();
    }

    private void Hit()
    {
        dead = true;
        anim.SetTrigger("IsDying");

        Destroy(this.GetComponent<BoxCollider>(),1);
        Destroy(this.GetComponent<Rigidbody>(),1);
        Destroy(this.gameObject, 4f);
    }
}
