﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    public GameObject bullet;
    public GameObject spawnPos;
    GameObject goob;

    float turnSpeed = 1.0f;


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "goober" && goob == null) 
        {
            goob = other.gameObject;
            InvokeRepeating("ShootBullet", 0, 3.0f);
        }
    }

    void ShootBullet() 
    {
        Instantiate(bullet, spawnPos.transform.position, spawnPos.transform.rotation);
        this.GetComponent<AudioSource>().Play();
        if (goob.GetComponent<Move>().dead) 
        {
            goob = null;
            CancelInvoke("ShootBullet");
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject == goob)
        {
            goob = null;
            CancelInvoke("ShootBullet");
        }
    }

    void Update()
    {
        if (goob != null) 
        {
            Vector3 direction = goob.transform.position - this.transform.position;
            this.transform.rotation = Quaternion.Slerp(
                this.transform.rotation, Quaternion.LookRotation(direction),
                turnSpeed * Time.smoothDeltaTime);
        }   
            
    }
}
